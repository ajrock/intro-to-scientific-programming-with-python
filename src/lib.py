import pandas as pd
import requests
import seaborn as sns


def data_download(url: str) -> str:
    """
    Quickly download data using requests
    :param url: a string containing a valid url with data
    :return: text field of the request download
    """
    print("retrieving data for you sir. Or ma'am. Or... look, i'm just a function, ok?")
    download = requests.get(url=url)
    print("downloading data")
    with open("dataset.csv", "w") as f:
        f.write(download.text)
    return download.text



def clean_dataset(df):
    print("cleaning ze data!")
    for col in NUMERIC_COLS:
        df[col] = pd.to_numeric(df[col], errors='coerce')
    df.dropna(inplace=True)
    key_map = {
        "SETOSA": "setosa",
        "vIrGiNiCa": "virginica",
        "tulip": None,
        "rose": None,
    }
    for key in key_map.keys():
        if key_map[key] is not None:
            df.loc[df.species == key, "species"] = key_map[key]
        else:
            df.drop(df[df.species == key].index, inplace=True)
    df.to_csv("temp.csv")
    print("ze data is clean! Mwah! *Chef's kiss*")
    return df


def pretty_picture(df):
    g = sns.PairGrid(df, hue="species")
    g.map_diag(sns.kdeplot, lw=3)
    g.map_upper(sns.scatterplot)
    g.map_lower(sns.kdeplot)
    g.save("a_pretty_picture.svg")
    print("I made you a pretty picture :3")
    return g
