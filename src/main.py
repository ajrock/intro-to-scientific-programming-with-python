import os
from io import StringIO

from lib import data_download, clean_dataset, pretty_picture

import pandas as pd


NUMERIC_COLS = ["sepal_length", "sepal_width", "petal_length", "petal_width"]


def print_hi(name):
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.



if __name__ == '__main__':
    print_hi("Pycharm")
    if os.path.exists("dataset.csv"):
        print("reading existing file")
        df = pd.read_csv("dataset.csv")
    else:
        print("need to download")
        result = data_download("https://www.andrewwilliamcurran.com/downloads/iris.csv")
        df = pd.read_csv(StringIO(result))

    df = clean_dataset(df)
    g = pretty_picture(df)
    print("blablah - shakespeare")
