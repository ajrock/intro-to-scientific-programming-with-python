# intro to scientific programming with python

Developed for CBBS workshop series on scientific programming.
Slides from the workshop and the scripts we developed together are included here. Download everything and give it a try! Packages listed in pipfile are required to make it work though.


## Installation
Recommendation is to use pipenv: https://pipenv.pypa.io/en/latest/ to set up your project. Once installed, use `pipenv install` to create your environment.

## Running
If using pipenv, you can run the script from your IDE.
Or, if you're a bash/shell kinda person, use
`pipenv run python -m main`
This will:
- download data
- clean it
- save the cleaned data
- make a graph
- save the graph

## RFF
REQUESTS FOR FEATURES:
Please check out how to submit feature requests via GitLab and submit there.
